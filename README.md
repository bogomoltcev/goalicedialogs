# goalicedialogs

This is simple example app, that demonstrating work with yandex dialogs

## Usage example

```
package main

import (
	"gitlab.com/bogomoltcev/goalicedialogs"
)

func main() {
	alice := goalicedialogs.Alice{
		Path: "/hooks",
		Port: ":9191",
	}
	alice.Start()
}
```

Skills dir contains files with a dialogue in JSON format