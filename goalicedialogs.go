package goalicedialogs

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/labstack/echo"
)

//Question struct of question
type Question struct {
	Variants []string `json:"variants"`
	Action   string   `json:"action"`
	Answer   string   `json:"answer"`
}

//Skill content structure
type Skill struct {
	SkillID   string            `json:"skillid"`
	Questions []Question        `json:"questions"`
	Answers   map[string]Answer `json:"answers"`
}

//Answer content structure
type Answer struct {
	Text string `json:"text"`
	Tts  string `json:"tts"`
}

//Alice main struct
type Alice struct {
	Path string
	Port string
}

//Response type
type Response struct {
	Text string `json:"text"`
	Tts  string `json:"tts"`
}

//Session type
type Session struct {
	New       bool   `json:"new"`
	MessageID int    `json:"message_id"`
	SessionID string `json:"session_id"`
	SkillID   string `json:"skill_id"`
	UserID    string `json:"user_id"`
}

//Markup type
type Markup struct {
	DangerousContext bool `json:"dangerous_context"`
}

//Request structure type for AliceRequest struature
type Request struct {
	Command           string `json:"command"`
	OriginalUtterance string `json:"original_utterance"`
	Type              string `json:"type"`
	Markup            Markup `json:"markup"`
	Payload           string `json:"payload"`
}

//Meta structure for alice request struct
type Meta struct {
	Locale   string `json:"locale"`
	Timezone string `json:"timezone"`
	ClientID string `json:"client_id"`
}

//AliceResponse structure
type AliceResponse struct {
	Response Response `json:"response"`
	Session  Session  `json:"session"`
	Version  string   `json:"version"`
}

//AliceRequest root structure
type AliceRequest struct {
	Meta    Meta
	Request Request
	Session Session
	Version string `json:"version"`
}

func getSkill(skillID string) Skill {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	s, err := ioutil.ReadFile(dir + "/skills/" + skillID)
	if err != nil {
		log.Printf("%s\n", err)
	}
	var skill Skill
	err = json.Unmarshal(s, &skill)
	if err != nil {
		log.Printf("%s\n", err)
	}
	return skill
}

func aliceRequest(c echo.Context) error {
	req := new(AliceRequest)
	err := c.Bind(req)
	if err != nil {
		log.Printf("%s\n", err)
	}
	skillName := c.Param("skill")
	log.Print(skillName + "\n")
	log.Print(req.Request.Command)
	res := new(AliceResponse)
	res.Version = req.Version
	res.Session.MessageID = req.Session.MessageID
	res.Session.SessionID = req.Session.SessionID
	res.Session.UserID = req.Session.UserID
	skill := getSkill(req.Session.SkillID)
	if req.Session.New == true {
		res.Response.Text = skill.Answers["welcome"].Text
		res.Response.Tts = skill.Answers["welcome"].Tts
	} else {
		flag := 0
	Loop:
		for k, v := range skill.Questions {
			for _, a := range v.Variants {
				if strings.ToLower(a) == strings.ToLower(req.Request.Command) {
					res.Response.Text = skill.Answers[skill.Questions[k].Answer].Text
					res.Response.Tts = skill.Answers[skill.Questions[k].Answer].Tts
					flag = 1
					break Loop
				}
			}
		}
		if flag == 0 {
			res.Response.Text = skill.Answers["nounderstand"].Text
			res.Response.Tts = skill.Answers["nounderstand"].Tts
		}
	}

	return c.JSON(http.StatusOK, res)
}

var session *mgo.Session

//Start alice dialog server
func (a *Alice) Start() {
	e := echo.New()
	e.POST(a.Path+"/:skill", aliceRequest)

	e.Logger.Fatal(e.Start(a.Port))
}
